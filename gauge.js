const Measured = require('measured-core')
const gauge = new Measured.Gauge(function() {
    return process.memoryUsage().rss;
});

setInterval(function() {
    console.log(gauge.toJSON());
}, 2000);

// 直接输出数字
// 21618688
// 22286336