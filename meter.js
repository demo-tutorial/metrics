// 重要: 需要配合yarn test 来一起看结果
const http  = require('http');
const stats = require('measured-core').createCollection();

http.createServer(function(req, res) {
  stats.meter('requestsPerSecond').mark();
  res.end('Thanks');
}).listen(3000);

setInterval(function() {
  console.log(stats.toJSON());
}, 1000);

// https://www.jianshu.com/p/5d3ea3be3122
// http://blog.itpub.net/69942496/viewspace-2665766/

// TPS: 是TransactionsPerSecond的缩写，也就是事务数/秒。简单理解成速率
// {
//   requestsPerSecond: {
//     mean: 17419.840974076073,            // 平均每秒TPS
//     count: 93270,                        // 总次数
//     currentRate: 8366.212132774697,      // 当前时间的TPS
//     '1MinuteRate': 1491.4914895046031,   // 最近1分钟的TPS
//     '5MinuteRate': 308.32350041154734,   // 最近5分钟的TPS
//     '15MinuteRate': 103.3459953166564    // 最近15分钟的TPSe
//   }
// }