const Measured = require('measured-core')

// 定时执行回调函数，并将回调函数的值返回
const gauge = new Measured.CachedGauge(() => {
    const value = Math.random()
    return new Promise(resolve => {
        resolve(value);
    });
}, 1);

setInterval(function() {
    console.log(gauge.toJSON());
}, 2000);

// 直接返回value
// 0.19412941554379204
// 0.9851641942902312
// 0.6162255418233487