const http  = require('http');
const Measured = require('measured-core')
const activeUploads = new Measured.Counter();

http.createServer(function(req, res) {
   activeUploads.inc();
   req.on('end', function() {
        // activeUploads.dec();
   });
   res.end('Thanks');
}).listen(3000);

setInterval(function() {
    console.log(activeUploads.toJSON());
}, 1000);