// 重要: 需要配合yarn test 来一起看结果 
const http  = require('http');
const Measured = require('measured-core')
const histogram = new Measured.Histogram();
http.createServer(function(req, res) {
  // 数据源更新，增量。timestamp defaults to Date.now().
  histogram.update(Math.random() * 100);
  res.end('Thanks');
}).listen(3000);

setInterval(function() {
  console.log("type:",histogram.getType());
  console.log(histogram.toJSON());
}, 1000);

// 数据来源为histogram.update的值
// {
//   min: 0.000253109239878313,
//   max: 99.9991442785384,
//   sum: 6019955.190764012,
//   mean: 50.108668287835755,
//   count: 120138,
//   median: 51.07776940850198, //中位数
//   p75: 75.55939147934407,
//   p95: 94.57790042599795,
//   p99: 98.74634252975642,
//   p999: 99.85568078930442
//   variance: 832.2531507544674, // 方差
//   stddev: 28.848798081626683,  // Standard Deviation: 标准差
// }