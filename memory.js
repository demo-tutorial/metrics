const os = require('os');
// 获取当前Node内存堆栈情况
// 获取系统空闲内存


function init() {
    // 不包含Swap的数据
    const sysFree = os.freemem();
    // 获取系统总内存
    const sysTotal = os.totalmem();
    const { rss, heapUsed, heapTotal } = process.memoryUsage();
    const memoryInfo = {
        // todo: 确认docker是否存在 Swap
        sys: 1 - sysFree / sysTotal,  // 系统内存占用率
        heap: heapUsed / heapTotal,   // Node堆内存占用率
        node: rss / sysTotal,         // Node占用系统内存的比例
    }

    console.log("memoryInfo:", memoryInfo)
}

setInterval(function() {
    init()
}, 2000);