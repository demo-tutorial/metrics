const Measured = require('measured-core')
const settableGauge = new Measured.SettableGauge();
setInterval(() => {
    const value = Math.random() * 100
    settableGauge.setValue(value);
}, 2000);

setInterval(() => {
    console.log(settableGauge.toJSON())
}, 3000);

// 不是json格式
// 75.10978450687243
// 78.64395131498867
// 11.442562686353487

