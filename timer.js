// 重要: 需要配合yarn test 来一起看结果
const http  = require('http');
const Measured = require('measured-core')
const timer = new Measured.Timer();
http.createServer(function(req, res) {
    const stopwatch = timer.start();
    req.on('end', function() {
        // 底层是process.hrtime
        const elapsed = stopwatch.end();
        timer.update(elapsed);
    });
    res.end('Thanks');
}).listen(3000);

setInterval(function() {
    console.log(timer.toJSON());
}, 1000);

// timer其实就是meter和histogram的结合体。并且可以方便获取到elapsed
// {
//     meter: {
//         mean: 18675.009101451356,
//         count: 112364,
//         currentRate: 41382.16855040188,
//         '1MinuteRate': 1122.22461402867,
//         '5MinuteRate': 231.98806274130553,
//         '15MinuteRate': 77.75935734247147
//     },
//     histogram: {
//         min: 0.01247899979352951,
//         max: 7.2961490005254745,
//         sum: 4241.313787214458,
//         variance: 0.00617256389147098,
//         mean: 0.03774619795676959,
//         stddev: 0.07856566611103721,
//         count: 112364,
//         median: 0.027915501967072487,
//         p75: 0.03154199756681919,
//         p95: 0.07458134815096855,
//         p99: 0.13043053805828117,
//         p999: 0.5504410125948496
//     }
// }