const Koa = require('koa');
const app = new Koa();

const { SelfReportingMetricsRegistry, LoggingReporter } = require('measured-reporting');
const { createProcessMetrics, createOSMetrics, createKoaMiddleware } = require('measured-node-metrics');

const registry = new SelfReportingMetricsRegistry(
    new LoggingReporter({
        logger: undefined, // defaults to new console logger if not supplied
        defaultDimensions: {
            hostname: require('os').hostname()
        }
    })
);

// Create and register default OS metrics
createOSMetrics(registry);
// Create and register default process metrics
createProcessMetrics(registry);

app.use(createKoaMiddleware(registry));

app.use(async ctx => {
  ctx.body = 'Hello World';
});

app.listen(3000);