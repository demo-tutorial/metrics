const os = require('os');
const process = require('process');
const { SelfReportingMetricsRegistry, Reporter } = require('measured-reporting');

class LoggingReporter extends Reporter {
  /**
   * @param {LoggingReporterOptions} [options]
   */
  constructor(options) {
    super(options);
    const level = (options || {}).logLevelToLogAt;
    this._logLevel = (level || 'info').toLowerCase();
  }

  /**
   * Logs the metrics via the inherited logger instance.
   * @param {MetricWrapper[]} metrics
   * @protected
   */
  _reportMetrics(metrics) {
    metrics.forEach(metric => {
      this._log[this._logLevel](
        JSON.stringify({
          metricName: metric.name,
          dimensions: this._getDimensions(metric),
          data: metric.metricImpl.toJSON()
        })
      );
    });
  }
}

// Create a self reporting registry with an anonymous Reporter instance;
const registry = new SelfReportingMetricsRegistry(
  new LoggingReporter({
    logger: undefined, // defaults to new console logger if not supplied
    defaultDimensions: {
      hostname: require('os').hostname()
    }
  })
);

// create a gauge that reports the process uptime every second
registry.getOrCreateGauge('node.process.uptime', () => process.uptime(), {}, 1);
registry.getOrCreateGauge('node.process.Math.random', () => Math.random(), {}, 1);
