const express = require('express');
const { SelfReportingMetricsRegistry, LoggingReporter } = require('measured-reporting');
const { createProcessMetrics, createOSMetrics, createExpressMiddleware } = require('measured-node-metrics');

const registry = new SelfReportingMetricsRegistry(
    new LoggingReporter({
        logger: undefined, // defaults to new console logger if not supplied
        defaultDimensions: {
            hostname: require('os').hostname()
        }
    })
);

// Create and register default OS metrics
createOSMetrics(registry);
// Create and register default process metrics
createProcessMetrics(registry);
// Use the express middleware
const app = express();
const port = 3000
app.use(createExpressMiddleware(registry));

app.get('/', (req, res) => {
    res.send('Hello World!')
})
  
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})